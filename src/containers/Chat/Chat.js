import React, {Component} from 'react';
import './Chat.css';
import AddMessageMenu from "../../components/AddMessageMenu/AddMessageMenu";
import Messages from "../../components/Messages/Messages";

class Chat extends Component {
  state = {
    currentAuthorValue: '',
    currentMessageValue: '',
    messages: [],
    lastMessageDateTime: null
  };

  getMessages = () => {
    let url = 'http://146.185.154.90:8000/messages';
    if (this.state.lastMessageDateTime) {
      url += '?datetime=' + this.state.lastMessageDateTime;
    }

    fetch(url).then(response => response.json()).then(messages => {
      if (messages.length > 0) {
        const messagesArray = [
          ...this.state.messages,
          ...messages
        ];

        this.setState({
          messages: messagesArray,
          lastMessageDateTime: messages[messages.length - 1].datetime
        });
      }
    });
  };

  componentDidMount() {
    this.getMessagesInterval = setInterval(this.getMessages, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.getMessagesInterval);
  }

  addMessage = (event, author, message) => {
    event.preventDefault();

    const data = new URLSearchParams();
    data.set('message', message);
    data.set('author', author);

    fetch('http://146.185.154.90:8000/messages', {
      method: 'POST',
      body: data
    });

    this.setState({
      currentMessageValue: ''
    });
  };

  onMessageChange = newValue => {
    this.setState({currentMessageValue: newValue});
  };

  onAuthorChange = newValue => {
    this.setState({currentAuthorValue: newValue});
  };

  render() {
    return (
      <div className="Chat">
        <AddMessageMenu
          currentAuthor={this.state.currentAuthorValue}
          onAuthorChange={this.onAuthorChange}
          currentMessage={this.state.currentMessageValue}
          onMessageChange={this.onMessageChange}
          addMessage={this.addMessage}
        />
        <Messages messagesArray={this.state.messages}/>
      </div>
    );
  }
}

export default Chat;
