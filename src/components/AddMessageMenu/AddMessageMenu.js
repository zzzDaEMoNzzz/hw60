import React from 'react';
import './AddMessageMenu.css';

const AddMessageMenu = props => {
  return (
    <form className="AddMessageMenu" onSubmit={event => props.addMessage(event, props.currentAuthor, props.currentMessage)}>
      <span>Author</span>
      <input
        type="text"
        value={props.currentAuthor}
        onChange={event => props.onAuthorChange(event.target.value)}
        required={true}
      />
      <span>Message:</span>
      <input
        type="text"
        value={props.currentMessage}
        onChange={event => props.onMessageChange(event.target.value)}
        required={true}
      />
      <button className="AddMessageMenu-postBtn">Add</button>
    </form>
  );
};

export default AddMessageMenu;
