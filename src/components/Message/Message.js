import React from 'react';
import './Message.css';

const Message = ({author, message, datetime}) => {
  const prependZero = num => {
    for (let i = 0; i < 2 - String(num).length; i++) {
      num = '0' + num;
    }
    return num;
  };

  const date = new Date(datetime);
  const time = prependZero(date.getDate()) + '.'
             + prependZero(date.getMonth() + 1) + ' '
             + prependZero(date.getHours()) + ':'
             + prependZero(date.getMinutes()) + ':'
             + prependZero(date.getSeconds());

  return (
    <div className="Message">
      <span className="Message-time">{time}</span>
      <span className="Message-author">{author}:</span>
      <span>{message}</span>
    </div>
  );
};

export default Message;
