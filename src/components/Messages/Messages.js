import React from 'react';
import './Messages.css';
import Message from "../Message/Message";

const Messages = props => {
  let messages = props.messagesArray.reduce((array, message) => {
    array.splice(0, 0, (
      <Message
        key={message._id}
        author={message.author}
        message={message.message}
        datetime={message.datetime}
      />
    ));

    return array;
  }, []);

  return (
    <div className="Messages">
      {messages}
    </div>
  );
};

export default Messages;
